#
# $Id: upgrade_latest.sql 6192 2013-01-05 12:31:23Z ajdonnison $
#
# DO NOT USE THIS SCRIPT DIRECTLY - USE THE INSTALLER INSTEAD.
#
# All entries must be date stamped in the correct format.
#

# 20130105
INSERT INTO `%dbprefix%config` VALUES (0, 'task_reminder_batch', 'false', 'task_reminder', 'checkbox');
ALTER TABLE `%dbprefix%event_queue` DROP `queue_module_type`, ADD `queue_batched` INTEGER NOT NULL DEFAULT '0', DROP KEY `queue_start`;
ALTER TABLE `%dbprefix%event_queue` ADD KEY `queue_start`(`queue_batched`, `queue_start`);

INSERT INTO `%dbprefix%user_preferences` VALUES ('0', 'USEDIGESTS', '0');
#
# Probably need to update all of the config variables
UPDATE `%dbprefix%config` SET `config_group` = 'auth' WHERE `config_name` IN ('username_min_len', 'password_min_len' );
UPDATE `%dbprefix%config` SET `config_group` = 'ui' WHERE `config_name` IN ('host_locale', 'currency_symbol', 'host_style', 'company_name', 'page_title', 'site_domain', 'email_prefix', 'admin_username', 'locale_warn', 'locale_alert', 'display_debug', 'restrict_color_selection', 'default_view_m', 'default_view_a', 'default_view_tab', 'log_changes', 'debug' );
UPDATE `%dbprefix%config` SET `config_group` = 'tasks' WHERE `config_name` IN ('check_overallocation', 'enable_gantt_charts', 'check_task_dates', 'check_task_empty_dynamic', 'daily_working_hours', 'link_tickets_kludge', 'show_all_task_assignees', 'restrict_task_time_editing', 'reset_memory_limit', 'direct_edit_assignment' );
UPDATE `%dbprefix%config` SET `config_group` = 'calendar' WHERE `config_name` IN ( 'cal_day_view_show_minical', 'cal_day_start', 'cal_day_end', 'cal_day_increment', 'cal_working_days');
UPDATE `%dbprefix%config` SET `config_group` = 'file' WHERE `config_name` IN ( 'parser_default', 'parser_application/msword', 'parser_text/html', 'parser_application/pdf', 'index_max_file_size', 'files_ci_preserve_attr', 'files_show_versions_edit' );

# 20131415 by isaac
ALTER TABLE `%dbprefix%billingcode` CHANGE COLUMN `billingcode_value` `billingcode_value` DECIMAL(10,2) NOT NULL DEFAULT '0.00'; 
ALTER TABLE `%dbprefix%users` ADD COLUMN `user_payrate` DECIMAL(10,2) NOT NULL DEFAULT 0.0  AFTER `user_signature`;

# 20131416 by isaac
INSERT INTO `%dbprefix%sysvals` ( `sysval_id` , `sysval_key_id` , `sysval_title` , `sysval_value` ) VALUES (null, 1, 'DurationType', '0|Hour\n1|Day\n2|Week\n3|Month\n4|Year');
INSERT INTO `%dbprefix%gacl_axo` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES (9, 'app', 'finance', 16, 'Financial Operation', 0);
ALTER TABLE `%dbprefix%users` ADD COLUMN `user_payduration` TINYINT(1) NOT NULL DEFAULT 0  AFTER `user_payrate`;
UPDATE `%dbprefix%sysvals` SET `sysval_value`='0|Unknown\n1|Administrative\n2|Operative\n3|Financial' WHERE `sysval_id`='6';
UPDATE `%dbprefix%sysvals` SET `sysval_value`='0|Unknown\n1|Administrative\n2|Operative\n3|Financial' WHERE `sysval_id`='7';

# 20131417 by isaac
INSERT INTO `%dbprefix%config` (`config_name`, `config_value`, `config_group`, `config_type`) VALUES ('weekly_working_days', '5', 'tasks', 'text');
INSERT INTO `%dbprefix%config` (`config_name`, `config_value`, `config_group`, `config_type`) VALUES ('monthly_working_days', '22', 'tasks', 'text');
INSERT INTO `%dbprefix%config` (`config_name`, `config_value`, `config_group`, `config_type`) VALUES ('yearly_working_days', '230', 'tasks', 'text');
ALTER TABLE `%dbprefix%task_log` ADD COLUMN `task_log_cost` DECIMAL(10,2) NOT NULL DEFAULT 0 COMMENT 'the cost wich the task incured'  AFTER `task_log_hours` ;

ALTER TABLE `%dbprefix%task_log` ADD COLUMN `task_log_labor_cost` DECIMAL(10,2) NOT NULL DEFAULT 0  AFTER `task_log_cost` ;


