<?php /* TASKS $Id: vw_log_update.php 6173 2012-06-19 11:40:11Z ajdonnison $ */

// post parameters:
//                  task_log_owner_id
//					last_log_date
//                  task_log_id[n]

if (!defined('DP_BASE_DIR')) {
	die('You should not access this file directly.');
}

global $AppUI, $task_id, $obj, $percent, $can_edit_time_information, $tab;

// check permissions
// XXX(issac): 'edit' => 'view'
if (!getPermission('tasks', 'view', $task_id)) {
	$AppUI->redirect('m=public&a=access_denied');
}

$canEdit = getPermission('task_log', 'edit', $task_id);
$canAdd = getPermission('task_log', 'add', $task_id);

$task_log_id = intval(dPgetParam($_GET, 'task_log_id', 0));
$log = new CTaskLog();
if ($task_log_id) {
	if (!($canEdit)) {
		$AppUI->redirect('m=public&a=access_denied');
	}
	$log->load($task_log_id);
} else {
	if (!($canAdd)) {
		$AppUI->redirect('m=public&a=access_denied');
	}
	$log->task_log_task = $task_id;
	$log->task_log_name = $obj->task_name;
}

// Check that the user is at least assigned to a task
$task = new CTask;
$task->load($task_id);
if (!($task->canAccess($AppUI->user_id))) {
	$AppUI->redirect('m=public&a=access_denied');
}
$log_date = new CDate();

if (isset($_POST['updatelog'])) {
	$log_count = intval(dPgetParam($_POST, 'tslg_count', 0));
	for ($i=0; $i<$log_count; $i++) {
		if (!isset($_POST['tslg_check_' . $i]))
			continue;
		$log = new CTaskLog();
		$id = intval(dPgetParam($_POST, 'tslg_check_' . $i, -1));
		if ($id > 0)
			$log->task_log_id = $id;
		$log->task_log_task = $task_id;
		$log->task_log_name = dPgetParam($_POST, 'tslg_name_' . $i, "N/A");
		$log->task_log_creator = dPgetParam($_POST, 'task_log_owner_id', $AppUI->user_id);
		$log->task_log_date = dPgetParam($_POST, 'tslg_date_' . $i, $log_date->format(FMT_DATETIME_MYSQL));
		$log->task_log_hours = dPgetParam($_POST, 'tslg_hours_' . $i, 0);
		$log->task_log_labor_cost = get_labor_cost($log->task_log_creator, $log->task_log_hours);
		$log->store();
	}
	
}

$q = new DBQuery;
$q->addTable('tasks', 't');
$q->innerJoin('projects', 'p', 'p.project_id = t.task_project');
$q->innerJoin('billingcode', 'b', 'b.company_id = p.project_company OR b.company_id = 0');
$q->addQuery('b.billingcode_id, b.billingcode_name');
$q->addWhere('b.billingcode_status = 0');
$q->addWhere('t.task_id = ' . $task_id);
$q->addOrder('b.billingcode_name');
$task_log_costcodes = $q->loadHashList();
$task_log_costcodes[0] = '';
$q->clear();

$taskLogReference = dPgetSysVal('TaskLogReference');

// Task Update Form
$df = $AppUI->getPref('SHDATEFORMAT');

//task log e-mail checkboxes
$tl = $AppUI->getPref('TASKLOGEMAIL');
$ta = $tl & 1;
$tt = $tl & 2;
$tp = $tl & 4;

//notify owner checkbox
$notify_own = $AppUI->getPref('MAILALL');

//task log e-mail list
$task_email_title = array();
$q->addTable('task_contacts', 'tc');
$q->leftJoin('contacts', 'c', 'c.contact_id = tc.contact_id');
$q->addWhere('tc.task_id = ' . $task_id);
$q->addQuery('tc.contact_id');
$q->addQuery('c.contact_first_name, c.contact_last_name');
$req =& $q->exec();
$cid = array();
for ($req; !($req->EOF); $req->MoveNext()) {
	$cid[] = $req->fields['contact_id'];
	$task_email_title[] = ($req->fields['contact_first_name'] . ' '
			. $req->fields['contact_last_name']);
}
$q->clear();

//project contacts
$q->addTable('project_contacts', 'pc');
$q->leftJoin('contacts', 'c', 'c.contact_id = pc.contact_id');
$q->addWhere('pc.project_id = ' . $obj->task_project);
$q->addQuery('pc.contact_id');
$q->addQuery('c.contact_first_name, c.contact_last_name');
$req =& $q->exec();
$cid = array();
$proj_email_title = array();
for ($req; !($req->EOF); $req->MoveNext()) {
	if (! in_array($req->fields['contact_id'], $cid)) {
		$cid[] = $req->fields['contact_id'];
		$proj_email_title[] = ($req->fields['contact_first_name'] . ' '
				. $req->fields['contact_last_name']);
	}
}


// get participants for this task
$q->clear();
$q->addTable("user_tasks","ut");
$q->innerJoin("users", "u", "u.user_id = ut.user_id");
$q->leftJoin("contacts", "c", "c.contact_id=u.user_contact");
$q->addQuery("ut.user_id, u.user_username as username,
		c.contact_first_name as firstname, c.contact_last_name as lastname");
$q->addWhere("ut.task_id = " . $task_id);
$task_participants = $q->loadList();
if (count($task_participants) == 0) {
	echo "No participants for this task";
	exit();
}

$task_log_owner_id = $task_participants[0]['user_id'];
// spell the full name for each user
foreach ($task_participants as &$p) {
	if ($p['firstname'] == null && $p['lastname'] == null) {
		$p['lastname'] = $p['username'];
	}
	$p['fullname'] = proper_full_name($p['firstname'], $p['lastname']);
	if (isset($_POST['task_log_owner_id']) 
			&& ($_POST['task_log_owner_id'] == $p['user_id'])) {
		$task_log_owner_id = $p['user_id'];
	}
}

$last_log_date = isset($_POST['last_log_date'])
						? new CDate($_POST['last_log_date']) : new CDate;
$last_log_date->setHourMinuteSecond(23, 59, 59);
$first_log_date = new CDate($last_log_date);
$first_log_date->addDays(-7);
$db_end = $last_log_date->format(FMT_DATETIME_MYSQL);
$db_start = $first_log_date->format(FMT_DATETIME_MYSQL);
$q->clear();
$q->addTable("task_log", "t");
$q->addQuery("t.*");
$q->addWhere("t.task_log_date > '$db_start'");
$q->addWhere("t.task_log_date <= '$db_end'");
$q->addWhere("t.task_log_creator = $task_log_owner_id");
$q->addWhere("t.task_log_task = $task_id");
$q->addOrder("task_log_date desc");
$db_task_logs = $q->loadHashList('task_log_id');

$populated_task_logs = array();
$cur_log_date = new CDate($last_log_date);
while ($cur_log_date->after($first_log_date)) {
	$set_flag = false;
	while ($db_log = array_shift($db_task_logs)) {
		$db_log_date = new CDate($db_log['task_log_date']);
		if ($db_log_date->getDay() == $cur_log_date->getDay()) {
			$set_flag = true;
			$populated_task_logs[] = array(
					    "is_new_log" => false,
						"task_log_id" => $db_log["task_log_id"],
						"task_log_name" => $db_log["task_log_name"],
			 			"task_log_hours" => $db_log["task_log_hours"],
			 			"task_log_date" => $db_log_date,
			);
		}
		else {
			array_unshift($db_task_logs, $db_log);
			break;
		}
	}

	if (!$set_flag) {
		$populated_task_logs[] = array(
				"is_new_log" => true,
				"task_log_id" => -1,
				"task_log_name" => "",
				"task_log_hours" => 0,
				"task_log_date" => new CDate($cur_log_date),
		);		
	}
	$cur_log_date->addDays(-1);
}
//echo var_dump($populated_task_logs);
?>

<!-- TIMER RELATED SCRIPTS -->
<script type="text/javascript">

function assortChanges(baseName) {
		if (document.getElementById("templa").checked == false)
			return;
		var n = parseInt(document.getElementById("tslg_count").value);
		var text = document.getElementById(baseName + "0").value;
		for (i=1; i<n; i++) {
			if (document.getElementById("tslg_check_" + i.toString()).checked == true)
			document.getElementById(baseName + i.toString()).value = text;
		}
	}

	function checkState(index) {
		if (document.getElementById("tslg_check_" + index.toString()).checked == true) {
			document.getElementById("tslg_hours_" + index.toString()).disabled = false;
			document.getElementById("tslg_name_" + index.toString()).disabled = false;
			var x = document.getElementById("tr_" + index.toString()).getElementsByTagName("td");
			for (var i=0; i<x.length; i++) {
				x[i].className = "";
			}
		}
		else {
			document.getElementById("tslg_hours_" + index.toString()).disabled = true;
			document.getElementById("tslg_name_" + index.toString()).disabled = true;
			var x = document.getElementById("tr_" + index.toString()).getElementsByTagName("td");
			for (var i=0; i<x.length; i++) {
				x[i].className = "disa";
			}			
		}
	}

	function submitFilterFrm() {
		f = document.filterFrm;
		f.submit();
	}

	function popCalendar2() {
		idate = eval('document.filterFrm.last_log_date.value');
		window.open('index.php?m=public&'+'a=calendar&'+'dialog=1&'+'callback=setCalendar2&'+'date='
					+ idate, 'calwin', 'width=251, height=220, scrollbars=no, status=no');
	}

	function setCalendar2(idate, fdate) {
		fld_date = eval('document.filterFrm.last_log_date');
		fld_fdate = eval('document.filterFrm.cal_log_date');
		fld_date.value = idate;
		fld_fdate.value = fdate;
		submitFilterFrm();
	}

	function changeTemplate() {
		if (document.getElementById("templa").checked == true) {
			document.getElementById("tslg_hours_0").className = "templ";
			document.getElementById("tslg_name_0").className = "templ";
		}
		else {
			document.getElementById("tslg_hours_0").className = "text";
			document.getElementById("tslg_name_0").className = "text";
		}
	}
	
</script>
<style>
  TABLE.tbl TD.disa { 
    background-color:#bebeaf;
  }
  .templ {
	 border: 3px groove;
	 font-family: Osaka,verdana,Sans-Serif;
	 font-size: 8pt;
  }
</style>
<!-- END OF TIMER RELATED SCRIPTS -->
<table border="0" cellspacing="1" cellpadding="2" width="100%">
	<tr>
		<td width="5%" valigh="top" align="right"><b><?php echo $AppUI->_("Owner"); ?> </b></td>
		<td width="90%">
		 <form name="filterFrm" 
		   action="?m=tasks&amp;a=view&amp;task_id=<?php echo $task_id; ?>&amp;tab=<?php echo $tab?>" method="post">
			 <select name=task_log_owner_id onchange="submitFilterFrm()">
			 <?php 
			  foreach($task_participants as $par) {
			  	$selected = $par['user_id'] == $task_log_owner_id ? ' selected="selected" ' : '';
			  	echo '<option value="' . $par['user_id'] . '"' . $selected . '>' . $par['fullname'] . '</option>';
			  }
			 ?>
			 </select>
			
			<input type="hidden" name="last_log_date" value="<?php echo $last_log_date->format(FMT_DATETIME_MYSQL); ?>" />
			<input type="text" name="cal_log_date" value="<?php 
			echo $last_log_date->format($df); ?>" class="text" disabled="disabled"  />
					<a href="#" onclick="popCalendar2()">
			          <img src="./images/calendar.gif" width="24" height="12" alt="<?php 
			echo $AppUI->_('Calendar'); ?>" border="0" /> </a>			 
		 </form>
		</td>
		<td></td>
	</tr>

  <form name="editFrm" action="?m=tasks&amp;a=view&amp;task_id=<?php echo $task_id; ?>&amp;tab=<?php echo $tab?>" method="post">
   <input type="hidden" name="updatelog" value="updatelog" />	
	<tr>
		<td width="5%" />
		<td width="90%" valign="top" align="center">
			<table border="0" cellpadding="2" cellspacing="1" width="100%"
				class="tbl">
				<tr>
					<th></th>
					<th><?php echo $AppUI->_('Week'); ?></th>
					<th><?php echo $AppUI->_('Date'); ?></th>
					<th><?php echo $AppUI->_('Hours'); ?></th>
					<th width="50%"><?php echo $AppUI->_('Summary'); ?></th>
					<th width="50">
					  <input type="checkbox" name="templa" id="templa" checked="checked" onchange="changeTemplate()" /><?php echo $AppUI->_("Template"); ?>
					</th>
				</tr>
				<?php 
				  $index = 0;
				  foreach ($populated_task_logs as &$tlog) {
				  	  echo '<tr id="tr_' . $index . '">' . "\n";
				  	  $checked = ' ';
				  	  $disabled = ' disabled="disabled" ';
				  	  $td_disa = ' class="disa" ';
				  	  if (($tlog['task_log_date']->isWorkingDay() && $tlog['task_log_id'] <= 0) || $index == 0) {
				  	  	$checked = ' checked="checked" ';
				  	  	$disabled = '';
				  	  	$td_disa = '';
				  	  }
					  echo '<td' . $td_disa . '><input type="checkbox" name="tslg_check_' . $index . 
							'" id="tslg_check_' . $index . '" value="' . $tlog['task_log_id'] . '"' . $checked . 
					        ' onclick="checkState(' . $index . ')" /></td>' . "\n";
					  echo '<td' . $td_disa . ' nowrap="nowrap">' . $AppUI->_($tlog['task_log_date']->format('%A')) . '</td>';
					  echo '<td' . $td_disa . '>' . $tlog['task_log_date']->format($df) . '</td>' . "\n";
					  $e1 = ($index == 0) ? 'onkeyup="assortChanges(\'tslg_hours_\')"' : '';
					  echo '<td' . $td_disa . '><input type="text" class="' . ($index == 0 ? 'templ' : 'text') . '" name="tslg_hours_' . $index . 
						   '" id="tslg_hours_' . $index . '" value="' . $tlog['task_log_hours'] . 
					       '"maxlength="8" size="4" ' . $disabled .
					        $e1 . ' /></td>' . "\n";
					  $e2 = ($index == 0) ? 'onkeyup="assortChanges(\'tslg_name_\')"' : '';
					  echo '<td' . $td_disa . ' width=50%><input type="text" class="' . ($index == 0 ? 'templ' : 'text') . '" name="tslg_name_' . $index .
						   '" id="tslg_name_' . $index . '" value="' . $tlog['task_log_name'] . 
					       '" maxlength="100" size="50" ' . $disabled . 
					        $e2 . '/></td>' . "\n";
					  echo '<td' . $td_disa . '>';
					  echo ($tlog['task_log_id'] <= 0 ? $AppUI->_('New') : '');
					  echo '<input type="hidden" name="tslg_date_' . $index . '" value="' . $tlog['task_log_date']->format(FMT_DATETIME_MYSQL) . '" />';
					  echo '</td>';
					  echo '</tr>' . "\n";
					  $index++;
				  } 
				  echo '<input type="hidden" name="tslg_count" id="tslg_count" value="' . $index . '" />';
				  echo '<input type="hidden" name="task_log_owner_id" value="' . $task_log_owner_id . '" />';
				  echo '<input type="hidden" name="last_log_date" value="' . $last_log_date->format(FMT_DATETIME_MYSQL) . '" />';
				  
				?>
			</table>

		</td>
		<td />
	</tr>
	<tr>
		<td />
		<td>
			<table border="0" width="100%">
				<tr>
					<td align="left">
					  <input type="button" class="button" value="<?php echo $AppUI->_('Cancel')?>" onclick="submitFilterFrm()">
					</td>
					<td align="right">
					  <input type="button" class="button" value="<?php echo $AppUI->_('Submit')?>" onclick="submit()">
					</td>
				</tr>
			</table>
		</td>
		<td />
	</tr>
  </form>
</table>
