<?php
if (!defined('DP_BASE_DIR')) {
  die('You should not access this file directly.');
}

function calc_user_daily_pay($hours, $pay_rate, $pay_duration)
{
	$hourly_rate = $pay_rate;
	$hours_per_day = dPgetConfig('daily_working_hours');
	if ($hours > $hours_per_day)
		$hours = $hours_per_day;
	
	switch ($pay_duration) {
		case 1:
			$hourly_rate = $pay_rate / $hours_per_day;
			break;
		case 2:
			$hourly_rate = $pay_rate / (dPgetConfig('weekly_working_days') * $hours_per_day);
			break;
		case 3:
			$hourly_rate = $pay_rate / (dPgetConfig('monthly_working_days') * $hours_per_day);
			break;
		case 4:
			$hourly_rate = $pay_rate / (dPgetConfig('yearly_working_days') * $hours_per_day);
			break;
		default:
			break;
	}
	
	return $hours * $hourly_rate;
}
?>